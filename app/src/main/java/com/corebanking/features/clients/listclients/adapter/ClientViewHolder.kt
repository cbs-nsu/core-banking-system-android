package com.corebanking.features.clients.listclients.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.corebanking.R
import com.corebanking.databinding.ItemClientBinding
import com.corebanking.features.clients.listclients.ListClientsViewModel
import com.corebanking.shared.clients.domain.entities.Client
import com.corebanking.shared.clients.domain.entities.ClientState
import java.text.SimpleDateFormat
import java.util.*

class ClientViewHolder(
    private val binding: ItemClientBinding,
    private val viewModel: ListClientsViewModel
) : RecyclerView.ViewHolder(binding.root) {

    companion object {
        fun from(
            parent: ViewGroup,
            viewModel: ListClientsViewModel
        ): ClientViewHolder {
            val layoutInflater = LayoutInflater.from(parent.context)
            val binding = ItemClientBinding.inflate(layoutInflater, parent, false)
            return ClientViewHolder(binding, viewModel)
        }
    }

    fun bind(item: Client) {
        with(binding) {
            fioText.text = item.fio
            dateText.text = SimpleDateFormat("dd.MM.yyyy", Locale("Rus")).format(item.dateBirth)
            stateText.text = when (item.state) {
                ClientState.STATE_NOT_TARIFF -> root.resources.getString(R.string.not_tariff)
                ClientState.STATE_NOT_CREDIT -> root.resources.getString(R.string.not_credit)
                ClientState.STATE_YES_CREDIT -> root.resources.getString(R.string.yes_credit)
                else -> root.resources.getString(R.string.locked)
            }
            root.setOnClickListener {
                viewModel.goToDetail(item.numberPassport)
            }
        }
    }
}