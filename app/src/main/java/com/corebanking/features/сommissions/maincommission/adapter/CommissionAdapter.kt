package com.corebanking.features.сommissions.maincommission.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.corebanking.features.сommissions.maincommission.MainCommissionsViewModel
import com.corebanking.shared.commissions.domain.entities.Commission

class CommissionAdapter(private val viewModel: MainCommissionsViewModel) :
    ListAdapter<Commission, CommissionViewHolder>(CommissionDiffCallback()) {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): CommissionViewHolder = CommissionViewHolder.from(parent)

    override fun onBindViewHolder(holder: CommissionViewHolder, position: Int) {
        holder.bind(getItem(position), viewModel)
    }
}

class CommissionDiffCallback : DiffUtil.ItemCallback<Commission>() {

    override fun areItemsTheSame(oldItem: Commission, newItem: Commission) =
        oldItem.id == newItem.id

    override fun areContentsTheSame(oldItem: Commission, newItem: Commission) = oldItem == newItem
}