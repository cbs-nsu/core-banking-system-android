package com.corebanking.shared.credits.data.datasource

import com.corebanking.shared.credits.domain.entities.Credit
import com.corebanking.shared.credits.domain.entities.CreditCreate

interface CreditsDataSource {

    suspend fun getActiveCreditByPassport(numberPassport: Long): Credit

    suspend fun getCreditById(id: Int): Credit

    suspend fun getHistoryCredits(numberPassport: Long): List<Credit>

    suspend fun saveCredit(creditCreateDto: CreditCreate)
}