package com.corebanking.shared.credits.data.api

import com.corebanking.shared.credits.domain.entities.Credit
import com.corebanking.shared.credits.domain.entities.CreditCreate
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

interface CreditsApi {

    @GET("/credit/active/{number}")
    suspend fun getActiveCreditByPassport(@Path("number") number: Long): Credit

    @GET("/credit/{id}")
    suspend fun getCreditById(@Path("id") id: Int): Credit

    @GET("/credit/history/{number}")
    suspend fun getHistoryCredits(@Path("number") number: Long): List<Credit>

    @POST("/credit")
    suspend fun saveCredit(@Body creditCreateDto: CreditCreate)
}