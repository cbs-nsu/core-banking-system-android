package com.corebanking.shared.credits.data.repository

import com.corebanking.shared.credits.data.datasource.CreditsDataSource
import com.corebanking.shared.credits.domain.entities.Credit
import com.corebanking.shared.credits.domain.entities.CreditCreate
import com.corebanking.shared.credits.domain.repository.CreditsRepository
import javax.inject.Inject

class CreditsRepositoryImpl @Inject constructor(
    private val dataSource: CreditsDataSource
) : CreditsRepository {

    override suspend fun getActiveCreditByPassport(numberPassport: Long): Credit =
        dataSource.getActiveCreditByPassport(numberPassport)

    override suspend fun getCreditById(id: Int): Credit =
        dataSource.getCreditById(id)

    override suspend fun getHistoryCredits(numberPassport: Long): List<Credit> =
        dataSource.getHistoryCredits(numberPassport)

    override suspend fun saveCredit(creditCreate: CreditCreate) =
        dataSource.saveCredit(creditCreate)
}