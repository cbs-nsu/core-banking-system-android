package com.corebanking.shared.credits.data.datasource

import com.corebanking.shared.credits.data.api.CreditsApi
import com.corebanking.shared.credits.data.datasource.CreditsDataSource
import com.corebanking.shared.credits.domain.entities.Credit
import com.corebanking.shared.credits.domain.entities.CreditCreate
import javax.inject.Inject

class CreditsDataSourceImpl @Inject constructor(
    private val api: CreditsApi
) : CreditsDataSource {

    override suspend fun getActiveCreditByPassport(numberPassport: Long): Credit =
        api.getActiveCreditByPassport(numberPassport)

    override suspend fun getCreditById(id: Int): Credit =
        api.getCreditById(id)

    override suspend fun getHistoryCredits(numberPassport: Long): List<Credit> =
        api.getHistoryCredits(numberPassport)

    override suspend fun saveCredit(creditCreateDto: CreditCreate) =
        api.saveCredit(creditCreateDto)
}