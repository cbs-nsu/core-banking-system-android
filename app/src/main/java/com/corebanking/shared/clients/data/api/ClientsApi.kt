package com.corebanking.shared.clients.data.api

import com.corebanking.shared.clients.domain.entities.Client
import com.corebanking.shared.clients.domain.entities.ClientFilters
import retrofit2.http.*

interface ClientsApi {

    @GET("/client/{numberPassport}")
    suspend fun getClientByPassport(@Path("numberPassport") numberPassport: Long): Client

    @GET("/client/block")
    suspend fun blockClient(@Query("number") number: Long, @Query("days") days: Int)

    @POST("client/search")
    suspend fun searchClients(@Body filters: ClientFilters): List<Client>

    @DELETE("/client/{numberPassport}")
    suspend fun deleteClient(@Path("numberPassport") numberPassport: Long)
}