package com.corebanking.shared.clients.domain.usecases

import com.corebanking.shared.clients.domain.entities.Client
import com.corebanking.shared.clients.domain.entities.ClientFilters
import com.corebanking.shared.clients.domain.repository.ClientsRepository
import javax.inject.Inject

class SearchClientsUseCase @Inject constructor(private val repository: ClientsRepository) {

    suspend operator fun invoke(filters: ClientFilters): List<Client> =
        repository.searchClients(filters)
}