package com.corebanking.shared.clients.domain.entities

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import java.io.Serializable

@JsonClass(generateAdapter = true)
data class ClientFilters(
    @Json(name = "fio") val fio: String?,
    @Json(name = "year") val year: Int?,
    @Json(name = "state") val state: List<ClientState>?
) : Serializable