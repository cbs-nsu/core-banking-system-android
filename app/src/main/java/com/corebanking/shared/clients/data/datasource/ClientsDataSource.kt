package com.corebanking.shared.clients.data.datasource

import com.corebanking.shared.clients.domain.entities.Client
import com.corebanking.shared.clients.domain.entities.ClientFilters

interface ClientsDataSource {

    suspend fun getClientByPassport(numberPassport: Long): Client

    suspend fun blockClient(number: Long, days: Int)

    suspend fun searchClients(filters: ClientFilters): List<Client>

    suspend fun deleteClient(numberPassport: Long)
}