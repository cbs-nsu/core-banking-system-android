package com.corebanking.shared.clients.di

import com.corebanking.shared.clients.data.api.ClientsApi
import com.corebanking.shared.clients.data.datasource.ClientsDataSource
import com.corebanking.shared.clients.data.datasource.ClientsDataSourceImpl
import com.corebanking.shared.clients.data.repository.ClientsRepositoryImpl
import com.corebanking.shared.clients.domain.repository.ClientsRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.FragmentComponent
import retrofit2.Retrofit

@Module
@InstallIn(FragmentComponent::class)
object ClientsModule {

    @Provides
    fun provideClientsRepository(
        clientsRepositoryImpl: ClientsRepositoryImpl
    ): ClientsRepository = clientsRepositoryImpl

    @Provides
    fun provideClientsDataSource(
        clientsDataSourceImpl: ClientsDataSourceImpl
    ): ClientsDataSource = clientsDataSourceImpl

    @Provides
    fun provideClientsApi(retrofit: Retrofit) =
        retrofit.create(ClientsApi::class.java)
}