package com.corebanking.shared.clients.data.datasource

import com.corebanking.shared.clients.data.api.ClientsApi
import com.corebanking.shared.clients.domain.entities.Client
import com.corebanking.shared.clients.domain.entities.ClientFilters
import javax.inject.Inject

class ClientsDataSourceImpl @Inject constructor(
    private val api: ClientsApi
) : ClientsDataSource {

    override suspend fun getClientByPassport(numberPassport: Long): Client =
        api.getClientByPassport(numberPassport)

    override suspend fun blockClient(number: Long, days: Int) {
        api.blockClient(number, days)
    }

    override suspend fun searchClients(filters: ClientFilters): List<Client> =
        api.searchClients(filters)

    override suspend fun deleteClient(numberPassport: Long) =
        api.deleteClient(numberPassport)
}