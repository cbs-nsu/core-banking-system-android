package com.corebanking.shared.clients.data.repository

import com.corebanking.shared.clients.data.datasource.ClientsDataSource
import com.corebanking.shared.clients.domain.entities.Client
import com.corebanking.shared.clients.domain.entities.ClientFilters
import com.corebanking.shared.clients.domain.repository.ClientsRepository
import javax.inject.Inject

class ClientsRepositoryImpl @Inject constructor(
    private val dataSource: ClientsDataSource
) : ClientsRepository {

    override suspend fun getByPassport(numberPassport: Long): Client =
        dataSource.getClientByPassport(numberPassport)

    override suspend fun blockClient(number: Long, days: Int) {
        dataSource.blockClient(number, days)
    }

    override suspend fun searchClients(filters: ClientFilters): List<Client> =
        dataSource.searchClients(filters)

    override suspend fun deleteClient(numberPassport: Long) =
        dataSource.deleteClient(numberPassport)
}