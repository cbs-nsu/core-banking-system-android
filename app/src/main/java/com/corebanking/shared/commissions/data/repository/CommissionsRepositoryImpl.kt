package com.corebanking.shared.commissions.data.repository

import com.corebanking.shared.commissions.data.datasource.CommissionsDataSource
import com.corebanking.shared.commissions.domain.entities.Commission
import com.corebanking.shared.commissions.domain.repository.CommissionsRepository
import javax.inject.Inject

class CommissionsRepositoryImpl @Inject constructor(
    private val dataSource: CommissionsDataSource
) : CommissionsRepository {

    override suspend fun getCommissions(): List<Commission> =
        dataSource.getCommissions()

    override suspend fun getCommissionById(id: Int): Commission =
        dataSource.getCommissionById(id)

    override suspend fun saveCommission(commission: Commission) {
        dataSource.saveCommission(commission)
    }

    override suspend fun deleteCommission(id: Int) {
        dataSource.deleteCommission(id)
    }
}