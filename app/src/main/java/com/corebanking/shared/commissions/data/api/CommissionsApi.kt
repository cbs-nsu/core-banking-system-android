package com.corebanking.shared.commissions.data.api

import com.corebanking.shared.commissions.domain.entities.Commission
import retrofit2.http.*

interface CommissionsApi {

    @GET("/commission")
    suspend fun getCommissions(): List<Commission>

    @GET("/commission/{id}")
    suspend fun getCommissionById(@Path("id") id: Int): Commission

    @POST("/commission")
    suspend fun saveCommission(@Body commissionDto: Commission)

    @DELETE("/commission/{id}")
    suspend fun deleteCommission(@Path("id") id: Int)
}