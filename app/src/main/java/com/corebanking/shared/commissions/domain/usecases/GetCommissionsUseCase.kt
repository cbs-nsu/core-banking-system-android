package com.corebanking.shared.commissions.domain.usecases

import com.corebanking.shared.commissions.domain.entities.Commission
import com.corebanking.shared.commissions.domain.repository.CommissionsRepository
import javax.inject.Inject

class GetCommissionsUseCase @Inject constructor(private val repository: CommissionsRepository) {

    suspend operator fun invoke(): List<Commission> = repository.getCommissions()
}