package com.corebanking.shared.commissions.data.datasource

import com.corebanking.shared.commissions.data.api.CommissionsApi
import com.corebanking.shared.commissions.domain.entities.Commission
import javax.inject.Inject

class CommissionsDataSourceImpl @Inject constructor(
    private val api: CommissionsApi
) : CommissionsDataSource {

    override suspend fun getCommissions(): List<Commission> =
        api.getCommissions()

    override suspend fun getCommissionById(id: Int): Commission =
        api.getCommissionById(id)

    override suspend fun saveCommission(commissionDto: Commission) {
        api.saveCommission(commissionDto)
    }

    override suspend fun deleteCommission(id: Int) {
        api.deleteCommission(id)
    }
}