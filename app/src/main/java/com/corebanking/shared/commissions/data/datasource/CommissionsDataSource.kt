package com.corebanking.shared.commissions.data.datasource

import com.corebanking.shared.commissions.domain.entities.Commission

interface CommissionsDataSource {

    suspend fun getCommissions(): List<Commission>

    suspend fun getCommissionById(id: Int): Commission

    suspend fun saveCommission(commissionDto: Commission)

    suspend fun deleteCommission(id: Int)
}