package com.corebanking.shared.commissions.domain.entities

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import java.io.Serializable

@JsonClass(generateAdapter = true)
data class Commission(
    @Json(name = "id") val id: Int,
    @Json(name = "name") val name: String,
    @Json(name = "interest") val interest: Int
) : Serializable