package com.corebanking.shared.tariffs.data.api

import com.corebanking.shared.tariffs.domain.entities.AvailableTariff
import com.corebanking.shared.tariffs.domain.entities.Tariff
import retrofit2.http.*

interface TariffsApi {

    @GET("/tariff")
    suspend fun getTariffs(): List<Tariff>

    @GET("/tariff/{id}")
    suspend fun getTariffById(@Path("id") id: Int): Tariff

    @GET("/tariff/availabletariff/{numberPassport}")
    suspend fun getTariffsByPassport(@Path("numberPassport") numberPassport: Long): List<Tariff>

    @GET("/tariff/availabletariff/{numberPassport}/not")
    suspend fun getTariffsNotByPassport(@Path("numberPassport") numberPassport: Long): List<Tariff>

    @POST("/tariff")
    suspend fun saveTariff(@Body tariffDto: Tariff)

    @POST("/tariff/availabletariff")
    suspend fun saveAvailableTariff(@Body availableTariffDto: AvailableTariff)

    @DELETE("/tariff/{id}")
    suspend fun deleteTariff(@Path("id") id: Int)
}