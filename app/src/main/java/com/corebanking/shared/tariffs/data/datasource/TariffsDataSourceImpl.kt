package com.corebanking.shared.tariffs.data.datasource

import com.corebanking.shared.tariffs.data.api.TariffsApi
import com.corebanking.shared.tariffs.domain.entities.AvailableTariff
import com.corebanking.shared.tariffs.domain.entities.Tariff
import javax.inject.Inject

class TariffsDataSourceImpl @Inject constructor(
    private val api: TariffsApi
) : TariffsDataSource {

    override suspend fun getTariffs(): List<Tariff> =
        api.getTariffs()

    override suspend fun getTariffById(id: Int): Tariff =
        api.getTariffById(id)

    override suspend fun getTariffsByPassport(numberPassport: Long): List<Tariff> =
        api.getTariffsByPassport(numberPassport)

    override suspend fun getTariffsNotByPassport(numberPassport: Long): List<Tariff> =
        api.getTariffsNotByPassport(numberPassport)

    override suspend fun saveTariff(tariffDto: Tariff) {
        api.saveTariff(tariffDto)
    }

    override suspend fun saveAvailableTariff(availableTariffDto: AvailableTariff) {
        api.saveAvailableTariff(availableTariffDto)
    }

    override suspend fun deleteTariff(id: Int) {
        api.deleteTariff(id)
    }
}