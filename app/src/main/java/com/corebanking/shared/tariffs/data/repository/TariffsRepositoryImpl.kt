package com.corebanking.shared.tariffs.data.repository

import com.corebanking.shared.tariffs.data.datasource.TariffsDataSource
import com.corebanking.shared.tariffs.domain.entities.AvailableTariff
import com.corebanking.shared.tariffs.domain.entities.Tariff
import com.corebanking.shared.tariffs.domain.repository.TariffsRepository
import javax.inject.Inject

class TariffsRepositoryImpl @Inject constructor(
    private val dataSource: TariffsDataSource
) : TariffsRepository {

    override suspend fun getTariffs(): List<Tariff> =
        dataSource.getTariffs()

    override suspend fun getTariffById(id: Int): Tariff =
        dataSource.getTariffById(id)

    override suspend fun getTariffsByPassport(numberPassport: Long): List<Tariff> =
        dataSource.getTariffsByPassport(numberPassport)

    override suspend fun getTariffsNotByPassport(numberPassport: Long): List<Tariff> =
        dataSource.getTariffsNotByPassport(numberPassport)

    override suspend fun saveTariff(tariff: Tariff) {
        dataSource.saveTariff(tariff)
    }

    override suspend fun saveAvailableTariff(availableTariff: AvailableTariff) {
        dataSource.saveAvailableTariff(availableTariff)
    }

    override suspend fun deleteTariff(id: Int) {
        dataSource.deleteTariff(id)
    }
}