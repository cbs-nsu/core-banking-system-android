package com.corebanking.shared.tariffs.data.datasource

import com.corebanking.shared.tariffs.domain.entities.AvailableTariff
import com.corebanking.shared.tariffs.domain.entities.Tariff

interface TariffsDataSource {

    suspend fun getTariffs(): List<Tariff>

    suspend fun getTariffById(id: Int): Tariff

    suspend fun getTariffsByPassport(numberPassport: Long): List<Tariff>

    suspend fun getTariffsNotByPassport(numberPassport: Long): List<Tariff>

    suspend fun saveTariff(tariffDto: Tariff)

    suspend fun saveAvailableTariff(availableTariffDto: AvailableTariff)

    suspend fun deleteTariff(id: Int)
}