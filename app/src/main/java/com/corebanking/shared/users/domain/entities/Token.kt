package com.corebanking.shared.users.domain.entities

import com.google.gson.annotations.SerializedName
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Token(
    @Json(name = "token") @SerializedName("token") val value: String
)
