package com.corebanking.shared.users.data.datasource

import com.corebanking.shared.users.domain.entities.Credentials
import com.corebanking.shared.users.domain.entities.Token

interface UsersDataSource {

    fun isTokenExist(): Boolean

    fun getToken(): Token

    fun saveToken(tokenDto: Token)

    fun clearToken()

    suspend fun auth(credentials: Credentials): Token
}