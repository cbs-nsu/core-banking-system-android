package com.corebanking.shared.users.data.datasource

import android.content.SharedPreferences
import androidx.core.content.edit
import com.corebanking.shared.users.data.api.UsersApi
import com.corebanking.libraries.utils.KeysArgsBundle.TOKEN
import com.corebanking.shared.users.domain.entities.Credentials
import com.corebanking.shared.users.domain.entities.Token
import javax.inject.Inject

class UsersDataSourceImpl @Inject constructor(
    private val api: UsersApi,
    private val sharedPreferences: SharedPreferences
) : UsersDataSource {

    override fun isTokenExist(): Boolean =
        !sharedPreferences.getString(TOKEN, null).isNullOrEmpty()

    override fun getToken(): Token = Token(
        sharedPreferences.getString(TOKEN, null) ?: throw IllegalStateException("Token doesn't exists")
    )

    override fun saveToken(tokenDto: Token) {
        sharedPreferences.edit(commit = true) {
            putString(TOKEN, tokenDto.value)
        }
    }

    override fun clearToken() {
        if (isTokenExist()) {
            sharedPreferences.edit(commit = true) {
                remove(TOKEN)
            }
        }
    }

    override suspend fun auth(credentials: Credentials): Token =
        api.auth(credentials)
}