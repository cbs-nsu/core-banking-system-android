package com.corebanking.shared.users.data.repository

import com.corebanking.shared.users.data.datasource.UsersDataSource
import com.corebanking.shared.users.domain.entities.Credentials
import com.corebanking.shared.users.domain.entities.Token
import com.corebanking.shared.users.domain.repository.UsersRepository
import javax.inject.Inject

class UsersRepositoryImpl @Inject constructor(
    private val dataSource: UsersDataSource
) : UsersRepository {

    override fun isTokenExist(): Boolean = dataSource.isTokenExist()

    override fun getToken(): Token = dataSource.getToken()

    override fun saveToken(token: Token) {
        dataSource.saveToken(token)
    }

    override fun clearToken() {
        dataSource.clearToken()
    }

    override suspend fun auth(credentials: Credentials): Token =
        dataSource.auth(credentials)
}