package com.corebanking.shared.users.data.api

import com.corebanking.shared.users.domain.entities.Credentials
import com.corebanking.shared.users.domain.entities.Token
import retrofit2.http.Body
import retrofit2.http.POST

interface UsersApi {

    @POST("/user/auth")
    suspend fun auth(@Body credentials: Credentials): Token
}