package com.corebanking.shared.users.domain.usecases

import com.corebanking.shared.users.domain.entities.Token
import com.corebanking.shared.users.domain.repository.UsersRepository
import javax.inject.Inject

class GetTokenUseCase @Inject constructor(private val repository: UsersRepository) {

    operator fun invoke(): Token = repository.getToken()
}