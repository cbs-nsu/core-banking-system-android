package com.corebanking.shared.users.domain.entities

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Credentials(
    @Json(name = "login") val login: String,
    @Json(name = "password") val password: String
)
