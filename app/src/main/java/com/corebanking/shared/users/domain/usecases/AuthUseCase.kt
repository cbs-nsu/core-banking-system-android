package com.corebanking.shared.users.domain.usecases

import com.corebanking.shared.users.domain.entities.Credentials
import com.corebanking.shared.users.domain.entities.Token
import com.corebanking.shared.users.domain.repository.UsersRepository
import javax.inject.Inject

class AuthUseCase @Inject constructor(private val repository: UsersRepository) {

    suspend operator fun invoke(credentials: Credentials): Token =
        repository.auth(credentials)
}